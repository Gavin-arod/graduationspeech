package cn.graduation.speech.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cn.graduation.speech.R;
import cn.graduation.speech.utils.DensityUtils;

/**
 * 随机产生不规律动画
 */

public class VoicePlayingAnim extends View {

    //画笔
    private Paint paint;

    private Random random;

    //跳动指针的集合
    private List<Pointer> pointers;

    //跳动指针的数量
    private int pointerNum;

    //逻辑坐标 原点
    private float basePointX;
    private float basePointY;

    //指针间的间隙  默认5dp
    private float pointerPadding;

    //每个指针的宽度 默认3dp
    private float pointerWidth;

    //指针的颜色
    private int pointerColor = Color.RED;

    //控制开始/停止
    private boolean isPlaying = false;

    //子线程
    private Thread myThread;

    //指针波动速率
    private int pointerSpeed;


    public VoicePlayingAnim(Context context) {
        super(context);
        initTypedArray(context, null);
        init();
    }

    public VoicePlayingAnim(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initTypedArray(context, attrs);
        init();
    }

    public VoicePlayingAnim(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initTypedArray(context, attrs);
        init();
    }

    private void initTypedArray(Context context, AttributeSet attrs) {
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.VoicePlayingAnim);
        pointerColor = ta.getColor(R.styleable.VoicePlayingAnim_pointer_color, Color.RED);
        pointerNum = ta.getInt(R.styleable.VoicePlayingAnim_pointer_num, 4);
        pointerWidth = DensityUtils.dp2px(getContext(), ta.getFloat(R.styleable.VoicePlayingAnim_pointer_width, 4f));
        pointerSpeed = ta.getInt(R.styleable.VoicePlayingAnim_pointer_speed, 40);
        ta.recycle();
    }

    /**
     * 初始化画笔与指针的集合
     */
    private void init() {
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(pointerColor);
        pointers = new ArrayList<>();

        random = new Random();
    }


    /**
     * 在onLayout中做一些，宽高方面的初始化
     *
     * @param changed changed
     * @param left    left
     * @param top     top
     * @param right   right
     * @param bottom  bottom
     */
    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        //获取逻辑原点的，也就是画布左下角的坐标。这里减去了paddingBottom的距离
        basePointY = getHeight() - getPaddingBottom();
        if (pointers != null) {
            pointers.clear();
        }
        if (isPlaying) {
            setPointHeight((float) (0.1 * (random.nextInt(10) + 1) * (getHeight() - getPaddingBottom() - getPaddingTop())));
        } else {
            setPointHeight(10f);
        }
        //计算每个指针之间的间隔  总宽度 - 左右两边的padding - 所有指针占去的宽度  然后再除以间隔的数量
        pointerPadding = (getWidth() - getPaddingLeft() - getPaddingRight() - pointerWidth * pointerNum) / (pointerNum - 1);
    }

    //设置指针高度
    private void setPointHeight(float height) {
        for (int i = 0; i < pointerNum; i++) {
            //创建指针对象，利用0~1的随机数 乘以 可绘制区域的高度。作为每个指针的初始高度。
            if (pointers != null) {
                pointers.add(new Pointer(height));
            }
        }
    }

    /**
     * 开始绘画
     *
     * @param canvas canvas
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //将x坐标移动到逻辑原点，也就是左下角
        basePointX = 0f + getPaddingLeft();
        //循环绘制每一个指针。
        for (int i = 0; i < pointers.size(); i++) {
            //绘制指针，也就是绘制矩形
            canvas.drawRect(basePointX,
                    basePointY - pointers.get(i).getHeight(),
                    basePointX + pointerWidth,
                    basePointY,
                    paint);
            basePointX += (pointerPadding + pointerWidth);
        }
    }

    /**
     * 开始播放动画
     */
    public void start() {
        if (!isPlaying) {
            //开启子线程
            if (myThread == null) {
                myThread = new Thread(new MyRunnable());
                myThread.start();
            }
            //控制子线程中的循环
            isPlaying = true;
        }
    }

    //停止子线程，并刷新画布
    public void stop() {
        isPlaying = false;
        invalidate();
    }

    //销毁子线程
    public void destroy() {
        isPlaying = false;
        if (myThread != null && myThread.isAlive()) {
            myThread.interrupt();
            myThread = null;
        }
    }

    /**
     * 动画正在播放
     */
    public boolean isPlaying() {
        return isPlaying;
    }

    /**
     * 处理子线程发出来的指令，然后刷新布局
     */
    private final Handler myHandler = new Handler(Looper.myLooper()) {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            invalidate();
        }
    };

    /**
     * 子线程，循环改变每个指针的高度
     */
    public class MyRunnable implements Runnable {

        @Override
        public void run() {
            //创建一个循环，每循环一次i+0.1
            for (float i = 0; i < Integer.MAX_VALUE; ) {
                try {
                    //循环改变每个指针高度
                    for (int j = 0; j < pointers.size(); j++) {
                        //利用正弦有规律的获取0~1的数
                        float rate = (float) Math.abs(Math.sin(i + j));
                        //rate 乘以 可绘制高度，来改变每个指针的高度
                        pointers.get(j).setHeight((basePointY - getPaddingTop()) * rate);
                    }
                    //休眠一下，可自行调节
                    Thread.sleep(pointerSpeed);
                    //控制开始/暂停
                    if (isPlaying) {
                        myHandler.sendEmptyMessage(0);
                        i += 0.1;
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    /**
     * 指针对象
     */
    public static class Pointer {
        private float height;

        public Pointer(float height) {
            this.height = height;
        }

        public float getHeight() {
            return height;
        }

        public void setHeight(float height) {
            this.height = height;
        }
    }
}


