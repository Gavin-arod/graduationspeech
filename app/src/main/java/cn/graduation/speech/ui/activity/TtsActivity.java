package cn.graduation.speech.ui.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.BackgroundColorSpan;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;

import com.iflytek.cloud.ErrorCode;
import com.iflytek.cloud.InitListener;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SpeechEvent;
import com.iflytek.cloud.SpeechSynthesizer;
import com.iflytek.cloud.SynthesizerListener;
import com.iflytek.cloud.msc.util.log.DebugLog;

import java.util.Vector;

import cn.graduation.speech.R;
import cn.graduation.speech.constants.Constants;
import cn.graduation.speech.speech.setting.TtsSettings;
import cn.graduation.speech.base.BaseActivity;
import cn.graduation.speech.ui.widget.VoicePlayingAnim;
import cn.graduation.speech.utils.ToastUtils;

/**
 * 语音合成activity
 */
public class TtsActivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = TtsActivity.class.getSimpleName();
    // 语音合成对象
    private SpeechSynthesizer mTts;

    private String[] mCloudSpeakersEntries;
    private String[] mCloudSpeakersValue;

    // 缓冲进度
    private int mPercentForBuffering = 0;
    // 播放进度
    private int mPercentForPlaying = 0;
    private SharedPreferences mSharedPreferences;

    private final Vector<byte[]> container = new Vector<>();

    private int selectedNum = 0;
    //默认发音人
    private String speaker = Constants.DEFAULT_SPEAKER;

    private AppCompatEditText etTtsText;
    private String texts;
    //动画组件
    private VoicePlayingAnim voicePlayingView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tts);
        initLayout();
        initListener();
        initTts();
    }

    @Override
    protected void onStart() {
        super.onStart();
        initTts();
    }

    /**
     * 初始化Layout。
     */
    private void initLayout() {
        findViewById(R.id.btn_pause_tts).setOnClickListener(this);
        findViewById(R.id.btn_keep_tts).setOnClickListener(this);
        findViewById(R.id.btn_cancel_tts).setOnClickListener(this);
        findViewById(R.id.btn_tts_person_select).setOnClickListener(this);
        findViewById(R.id.btn_tts_replay).setOnClickListener(this);
        findViewById(R.id.image_tts_set).setOnClickListener(this);
        etTtsText = findViewById(R.id.et_tts_text);
        voicePlayingView = findViewById(R.id.voice_speaking_anim);

        Editable editable = etTtsText.getText();
        if (editable != null) {
            texts = editable.toString();
        }
    }

    //输入框输入监听
    private void initListener() {
        etTtsText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                texts = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        //editText获取焦点时
        etTtsText.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                //输入内容时，取消语音合成
                stopSpeaking();
            } else {
                setTexts();
            }
        });
    }

    private void initTts() {
        // 初始化合成对象
        if (null == mTts) {
            mTts = SpeechSynthesizer.createSynthesizer(TtsActivity.this, mTtsInitListener);
        }
        // 云端发音人名称列表
        mCloudSpeakersEntries = getResources().getStringArray(R.array.voicer_cloud_entries);
        mCloudSpeakersValue = getResources().getStringArray(R.array.voicer_cloud_values);

        mSharedPreferences = getSharedPreferences(TtsSettings.PREFER_NAME, MODE_PRIVATE);
    }

    /**
     * 初始化监听。
     */
    private final InitListener mTtsInitListener = code -> {
        Log.d(TAG, "InitListener init() code = " + code);
        if (code != ErrorCode.SUCCESS) {
            ToastUtils.showTip(TtsActivity.this, "初始化失败,错误码：" + code + ",请点击网址https://www.xfyun.cn/document/error-code查询解决方案");
        } else {
            // 初始化成功，之后可以调用startSpeaking方法
            // 注：有的开发者在onCreate方法中创建完合成对象之后马上就调用startSpeaking进行合成，
            // 正确的做法是将onCreate中的startSpeaking调用移至这里

            startSpeech();
        }
    };

    //开始语音
    private void startSpeech() {
        // 初始化合成对象
        if (null == mTts) {
            mTts = SpeechSynthesizer.createSynthesizer(TtsActivity.this, mTtsInitListener);
        }

        // 设置参数
        setParam();
        int statusCode = mTts.startSpeaking(texts, mTtsListener);
        if (statusCode != ErrorCode.SUCCESS) {
            ToastUtils.showTip(TtsActivity.this, "语音合成失败,错误码: " + statusCode + ",请点击网址https://www.xfyun.cn/document/error-code查询解决方案");
        }
    }

    @Override
    public void onClick(View v) {
        if (null == mTts) {
            // 创建单例失败，与 21001 错误为同样原因，参考 http://bbs.xfyun.cn/forum.php?mod=viewthread&tid=9688
            ToastUtils.showTip(TtsActivity.this, "创建对象失败，请确认 libmsc.so 放置正确，且有调用 createUtility 进行初始化");
            return;
        }

        int viewId = v.getId();
        if (viewId == R.id.image_tts_set) {
            //设置
            if (SpeechConstant.TYPE_CLOUD.equals(Constants.TYPE_ENGINE)) {
                Intent intent = new Intent(TtsActivity.this, TtsSettings.class);
                startActivity(intent);
            } else {
                ToastUtils.showTip(TtsActivity.this, "请前往xfyun.cn下载离线合成体验");
            }
        } else if (viewId == R.id.btn_pause_tts) {
            //暂停播放
            mTts.pauseSpeaking();
        } else if (viewId == R.id.btn_cancel_tts) {
            //取消合成
            stopSpeaking();
        } else if (viewId == R.id.btn_keep_tts) {
            //继续播放
            mTts.resumeSpeaking();
        } else if (viewId == R.id.btn_tts_replay) {
            //重新播放
            startSpeech();
        } else if (viewId == R.id.btn_tts_person_select) {
            //选择发音人
            showPersonSelectDialog();
        }
    }

    /**
     * 发音人选择。
     */
    private void showPersonSelectDialog() {
        // 选择在线合成
        new AlertDialog.Builder(this)
                .setTitle("在线合成发音人列表")
                .setSingleChoiceItems(mCloudSpeakersEntries, selectedNum, (dialog, which) -> {
                    // 点击单选框后的处理
                    speaker = mCloudSpeakersValue[which];
                    if ("catherine".equals(speaker) ||
                            "henry".equals(speaker) ||
                            "vimary".equals(speaker)) {
                        etTtsText.setText(R.string.text_tts_source_en);
                    } else {
                        etTtsText.setText(R.string.text_tts_source);
                    }
                    setTexts();
                    selectedNum = which;

                    //重置播放
                    startSpeech();

                    dialog.dismiss();
                }).show();
    }

    private void setTexts() {
        if (etTtsText == null) {
            return;
        }
        Editable editable = etTtsText.getText();
        if (editable != null) {
            texts = editable.toString();
        }
    }


    /**
     * 合成回调监听。
     */
    private final SynthesizerListener mTtsListener = new SynthesizerListener() {

        @Override
        public void onSpeakBegin() {
            showAnimView();
            ToastUtils.showTip(TtsActivity.this, "开始播放");
        }

        @Override
        public void onSpeakPaused() {
            hideAnimView();
            ToastUtils.showTip(TtsActivity.this, "暂停播放");
        }

        @Override
        public void onSpeakResumed() {
            showAnimView();
            ToastUtils.showTip(TtsActivity.this, "继续播放");
        }

        @Override
        public void onBufferProgress(int percent, int beginPos, int endPos,
                                     String info) {
            // 合成进度
            Log.e("MscSpeechLog_", "percent =" + percent);
            mPercentForBuffering = percent;
//            ToastUtils.showTip(TtsActivity.this,
//                    String.format(getString(R.string.tts_toast_format),
//                            mPercentForBuffering, mPercentForPlaying));

        }

        @Override
        public void onSpeakProgress(int percent, int beginPos, int endPos) {
            // 播放进度
            Log.e("MscSpeechLog_", "percent =" + percent);
            mPercentForPlaying = percent;
//            ToastUtils.showTip(TtsActivity.this,
//                    String.format(getString(R.string.tts_toast_format),
//                            mPercentForBuffering, mPercentForPlaying));

            SpannableStringBuilder style = new SpannableStringBuilder(texts);
            Log.e(TAG, "beginPos = " + beginPos + "  endPos = " + endPos);
            style.setSpan(new BackgroundColorSpan(Color.YELLOW), beginPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            etTtsText.setText(style);
        }

        @Override
        public void onCompleted(SpeechError error) {
            System.out.println("onCompleted");
            if (error == null) {
                DebugLog.LogD("播放完成," + container.size());
                ToastUtils.showTip(TtsActivity.this, "播放完成！");
                hideAnimView();
            } else {
                ToastUtils.showTip(TtsActivity.this, error.getPlainDescription(true));
            }
        }

        @Override
        public void onEvent(int eventType, int arg1, int arg2, Bundle obj) {
            //	 以下代码用于获取与云端的会话id，当业务出错时将会话id提供给技术支持人员，可用于查询会话日志，定位出错原因
            //	 若使用本地能力，会话id为null
            if (SpeechEvent.EVENT_SESSION_ID == eventType) {
                String sid = obj.getString(SpeechEvent.KEY_EVENT_SESSION_ID);
                Log.d(TAG, "session id =" + sid);
            }

            //当设置SpeechConstant.TTS_DATA_NOTIFY为1时，抛出buf数据
            if (SpeechEvent.EVENT_TTS_BUFFER == eventType) {
                byte[] buf = obj.getByteArray(SpeechEvent.KEY_EVENT_TTS_BUFFER);
                Log.e("MscSpeechLog_", "bufis =" + buf.length);
                container.add(buf);
            }
        }
    };

    /**
     * 参数设置
     */
    private void setParam() {
        // 清空参数
        mTts.setParameter(SpeechConstant.PARAMS, null);
        // 根据合成引擎设置相应参数
        if (Constants.TYPE_ENGINE.equals(SpeechConstant.TYPE_CLOUD)) {
            mTts.setParameter(SpeechConstant.ENGINE_TYPE, SpeechConstant.TYPE_CLOUD);
            //支持实时音频返回，仅在synthesizeToUri条件下支持
            mTts.setParameter(SpeechConstant.TTS_DATA_NOTIFY, "1");
            //	mTts.setParameter(SpeechConstant.TTS_BUFFER_TIME,"1");

            // 设置在线合成发音人
            mTts.setParameter(SpeechConstant.VOICE_NAME, speaker);
            //设置合成语速
            mTts.setParameter(SpeechConstant.SPEED, mSharedPreferences.getString("speed_preference", "50"));
            //设置合成音调
            mTts.setParameter(SpeechConstant.PITCH, mSharedPreferences.getString("pitch_preference", "50"));
            //设置合成音量
            mTts.setParameter(SpeechConstant.VOLUME, mSharedPreferences.getString("volume_preference", "50"));
        } else {
            mTts.setParameter(SpeechConstant.ENGINE_TYPE, SpeechConstant.TYPE_LOCAL);
            mTts.setParameter(SpeechConstant.VOICE_NAME, "");

        }

        //设置播放器音频流类型
        mTts.setParameter(SpeechConstant.STREAM_TYPE, mSharedPreferences.getString("stream_preference", "3"));
        // 设置播放合成音频打断音乐播放，默认为true
        mTts.setParameter(SpeechConstant.KEY_REQUEST_FOCUS, "false");
    }

    //取消播放
    private void stopSpeaking() {
        if (mTts != null) {
            mTts.stopSpeaking();
        }
        hideAnimView();
        ToastUtils.showTip(TtsActivity.this, "取消播放");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (null != mTts) {
            //继续语音
            mTts.resumeSpeaking();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (null != mTts) {
            //暂停语音
            mTts.pauseSpeaking();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        releaseSpeak();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releaseSpeak();
    }

    //释放语音合成资源
    private void releaseSpeak() {
        if (null != mTts) {
            mTts.stopSpeaking();
            // 退出时释放连接
            mTts.destroy();
            mTts = null;
        }

        //释放动画资源
        if (voicePlayingView != null && voicePlayingView.isPlaying()) {
            voicePlayingView.destroy();
        }
    }

    //开始动画
    private void showAnimView() {
        if (voicePlayingView != null && !voicePlayingView.isPlaying()) {
            voicePlayingView.start();
            voicePlayingView.setVisibility(View.VISIBLE);
        }
    }

    //停止动画
    private void hideAnimView() {
        if (voicePlayingView != null && voicePlayingView.isPlaying()) {
            voicePlayingView.stop();
            voicePlayingView.setVisibility(View.GONE);
        }
    }

}
