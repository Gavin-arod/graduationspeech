package cn.graduation.speech.utils;

import android.content.Context;
import android.widget.Toast;

public class ToastUtils {
    public static void showTip(Context context, String str) {
        Toast.makeText(context, str, Toast.LENGTH_SHORT).show();
    }
}
